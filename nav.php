<?php

	require_once('function.php');

	$navigation = [
		'#main' => 'Главная',
		'#sativa' => 'Сатива',
		'#indika' => 'Индика',
		'#gash' => 'Гашиш',
		'#contacts' => 'Контакты',
	];
	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Навигация(Green Friday)</title>
	<style>
		nav {display: flex;}
		nav div {padding: 10px;}
		nav div a {color: green;}
		nav div a:hover {text-decoration: none;}
	</style>
</head>
<body>
	<nav>
		<?php
		showNav($navigation);
		?>
	</nav>
</body>
</html>